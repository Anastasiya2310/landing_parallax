var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var less = require('gulp-less');
var autoprefixer = require('autoprefixer');
var del = require('del');


gulp.task('less', ['clean'], function () {
  gulp.src('./less/style.less')
    .pipe(less({}))
    .pipe(autoprefixer({}))
    .pipe(minifyCss())
    .pipe(gulp.dest('./css'));
});

gulp.task('clean', function () {
	del(['css']);
});

gulp.task('watch', function () {
    gulp.watch('./less/**/*.less', ['less']);
});

gulp.task('default', ['less', 'watch']);